<?php 
require 'vendor/autoload.php';
$mongo = new MongoDB\Client("mongodb://127.0.0.1:27017");
$db = $mongo->restaurants;
$collection = $db->restaurants;
?>

<!doctype html>
<html lang="fr">
  <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
     <title>Restaurant</title>
  </head>
  <body>

<?php

$id = $_GET['fiche'];
$all = $collection->find(
        ['_id' => new MongoDB\BSON\ObjectId($id)],
);

?>

<div class="container w-75 my-5">
<table class="table table-dark">
  <thead>

<?php
   foreach ($all as $restaurant) 
   {
     echo " <tbody>
            <tr>
            <th scope=\"col\">Nom</th>
            <th scope=\"col\">Style de cuisine</th>
            <th scope=\"col\">Site Web</th>
            <th scope=\"col\">Numéro de téléphone</th>
            <th scope=\"col\">Prix Moyen</th>
            <th scope=\"col\">Supprimer</th>
            </tr>
         </thead>
            <tr>
            <td>" . $restaurant['name'] . "</td>
            <td>" . $restaurant['cuisine'] . "</td>
            <td><a href=\"https://" . $restaurant['site'] . "\" tarPOST=\"_blank\">" . $restaurant['site']."</a></td>
            <td>".$restaurant['telephone']."</td>
            <td>".$restaurant['prix']." €</td>
            <td><a href=\"?delete=$restaurant->_id\">Supprimer</a></td>
         </tr>
         <tr><th>Adresse</th></tr>
         <tr>
            <td>" . $restaurant['numero'] . "</td>
            <td>" . $restaurant['rue'] . "</td>
            <td>" . $restaurant['ville'] . " - " . $restaurant['postale'] ."</td>
         </tr>
         <tr><th>Coordonnées GPS</th></tr>
         <tr>
            <td>Latitude : " . $restaurant['latitude'] . "</td>
            <td>Longitude" . $restaurant['longitude'] . "</td>
            <td><a href=\"https://www.openstreetmap.org/#map=18/" . $restaurant['latitude'] . "/" . $restaurant['longitude'] . "\" tarPOST=\"_blank\">Voir sur une carte</a></td>
         </tr>";
   }
?>
   </tbody>
</table>
</div>


<div class="container w-50 my-5">
  <form action="" method="POST">
<div>
   <input type="hidden" name="id" value="<?php echo $id ?> ">
</div>
  <div class="form-row">
    <div class="form-group col-md-4">
    <label for="nom">Nom du restaurant : </label>
    <input type="text" class="form-control" id="nom" value="<?php echo $restaurant['name']?>" name="nomRestaurant"  placeholder="<?php echo $restaurant['name'] ?>">
  </div>

  <div class="form-group col-md-8">
    <label for="site">Site web du restaurant : </label>
    <input type="text" class="form-control" id="site" name="siteWeb" value="<?php echo $restaurant['site'] ?>" placeholder="<?php echo $restaurant['site'] ?>">
  </div>
  </div>

  <div class="form-row">
  <div class="form-group col-md-4">
    <label for="cuisine">Style de cuisine : </label>
      <select class="form-control" id="cuisine" name="cuisine" value="<?php echo $restaurant['cuisine'] ?>" placeholder="<?php echo $restaurant['cuisine'] ?> ">
         <option value="Française">Française</option>
         <option value="Italienne">Italienne</option>
         <option value="Japonaise">Japonaise</option>
         <option value="Indienne">Indienne</option>
         <option value="Africaine">Africaine</option>
      </select>
  </div>

    <div class="form-group col-md-4">
      <label for="prix">Prix moyen : </label>
      <input type="number" class="form-control" id="prix" name="prix" value="<?php echo $restaurant['prix'] ?>" placeholder="<?php echo $restaurant['prix'] ?>">
   </div>
  <div class="form-group col-md-4">
      <label for="telephone">Téléphone : </label>
      <input type="text" class="form-control" id="telephone" name="telephone" value="<?php echo $restaurant['telephone'] ?>" placeholder="<?php echo $restaurant['telephone'] ?>">
   </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-4">
    <label for="numero">Numéro de la rue : </label>
    <input type="text" class="form-control" id="numero" name="numero" value="<?php echo $restaurant['numero'] ?>" placeholder="<?php echo $restaurant['numero'] ?> ">
  </div>
  <div class="form-group col-md-8">
    <label for="rue">Nom de la rue : </label>
    <input type="text" class="form-control" id="rue" name="rue" value="<?php echo $restaurant['rue'] ?>" placeholder="<?php echo $restaurant['rue'] ?> ">
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-8">
      <label for="ville">Ville : </label>
      <input type="text" class="form-control" id="ville" name="ville" value="<?php echo $restaurant['ville'] ?>" placeholder="<?php echo $restaurant['ville'] ?> ">
    </div>
    <div class="form-group col-md-4">
      <label for="postale">Code postale : </label>
      <input type="text" class="form-control" id="postale" name="postale" value="<?php echo $restaurant['postale'] ?>" placeholder="<?php echo $restaurant['postale'] ?> ">
   </div>
  </div>

   <div class="form-row">
   <div class="form-group col-md-6">
      <label for="latitude">Latitude</label>
      <input type="text" class="form-control" id="latitude" name="latitude" value="<?php echo $restaurant['latitude'] ?>" placeholder="<?php echo $restaurant['latitude'] ?> ">
    </div>
    <div class="form-group col-md-6">
      <label for="longitude">Longitude</label>
      <input type="text" class="form-control" id="longitude" name="longitude" value="<?php echo $restaurant['longitude'] ?>" placeholder="<?php echo $restaurant['longitude'] ?> ">
    </div>
  </div>

  <button type="submit" class="btn btn-primary mt-4" name="modifier">Modifier</button>
</form>
</div>


<?php 

if (isset($_POST['modifier'])) 
{
   $collection->updateOne(
       ['_id' => new MongoDB\BSON\ObjectId($id)],
       ['$set' =>
           [
            'name' => $_POST['nomRestaurant'],
            'cuisine' => $_POST['cuisine'],
            'site' => $_POST['siteWeb'],
            'numero' => $_POST['numero'],
            'rue' => $_POST['rue'],
            'ville' => $_POST['ville'],
            'postale' => $_POST['postale'],
            'prix' => $_POST['prix'],
            'telephone' => $_POST['telephone'],
            'latitude' => $_POST['latitude'],
            'longitude' => $_POST['longitude']
           ]
       ]
   );

}

?>

     <!-- Optional JavaScript -->
     <!-- jQuery first, then Popper.js, then Bootstrap JS -->
     <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>