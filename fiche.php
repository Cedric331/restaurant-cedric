<?php 
require 'vendor/autoload.php';
$mongo = new MongoDB\Client("mongodb://127.0.0.1:27017");
$db = $mongo->restaurants;
$collection = $db->restaurants;
?>

<!doctype html>
<html lang="fr">
  <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
     <title>Restaurant</title>
  </head>
  <body>

<?php

if(isset($_POST['delete']))
{
    $id = $_POST['delete'];
    $collection->deleteOne(
        ['_id' => new MongoDB\BSON\ObjectId($id)]
    );
    header("Location: index.php");
}


$id = $_POST['fiche'];
$all = $collection->find(
        ['_id' => new MongoDB\BSON\ObjectId($id)],
);

?>

<div class="container w-75 my-5">
<table class="table table-dark">
  <thead>

<?php
   foreach ($all as $restaurant) 
   {
     echo " <tbody>
            <tr>
            <th scope=\"col\">Nom</th>
            <th scope=\"col\">Style de cuisine</th>
            <th scope=\"col\">Site Web</th>
            <th scope=\"col\">Numéro de téléphone</th>
            <th scope=\"col\">Prix Moyen</th>
            <th scope=\"col\">Supprimer</th>
            </tr>
         </thead>
            <tr>
            <td>" . $restaurant['name'] . "</td>
            <td>" . $restaurant['cuisine'] . "</td>
            <td><a href=\"https://" . $restaurant['site'] . "\" tarPOST=\"_blank\">" . $restaurant['site']."</a></td>
            <td>".$restaurant['telephone']."</td>
            <td>".$restaurant['prix']." €</td>


            <td><a href=\"?delete=$restaurant->_id\">Supprimer</a></td>


            
         </tr>
         <tr><th>Adresse</th></tr>
         <tr>
            <td>" . $restaurant['numero'] . "</td>
            <td>" . $restaurant['rue'] . "</td>
            <td>" . $restaurant['ville'] . " - " . $restaurant['postale'] ."</td>
         </tr>
         <tr><th>Coordonnées GPS</th></tr>
         <tr>
            <td>Latitude : " . $restaurant['latitude'] . "</td>
            <td>Longitude" . $restaurant['longitude'] . "</td>
            <td><a href=\"https://www.openstreetmap.org/#map=18/" . $restaurant['latitude'] . "/" . $restaurant['longitude'] . "\" tarPOST=\"_blank\">Voir sur une carte</a></td>
         </tr>";
   }
?>
   </tbody>
</table>
<?php 
          echo  "<form action=\"editer.php\" method=\"GET\">
                 <input type=\"hidden\" name=\"fiche\"value=". $restaurant->_id . ">
                 <input type=\"submit\" class=\"btn btn-primary\" value=\"Editer\">
                 </form>";
?>
</div>


     <!-- Optional JavaScript -->
     <!-- jQuery first, then Popper.js, then Bootstrap JS -->
     <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>