<?php 
require '../vendor/autoload.php';
$mongo = new MongoDB\Client("mongodb://127.0.0.1:27017");
$db = $mongo->restaurants;
$collection = $db->restaurants;
?>

<!doctype html>
<html lang="fr">
  <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
     <title>Restaurant</title>
  </head>
  <body>

<div class="container w-50 my-5">
  <form action="" method="POST">

  <div class="form-row">
    <div class="form-group col-md-4">
    <label for="nom">Nom du restaurant : </label>
    <input type="text" class="form-control" id="nom" name="nomRestaurant">
  </div>

  <div class="form-group col-md-8">
    <label for="site">Site web du restaurant : </label>
    <input type="text" class="form-control" id="site" name="siteWeb">
  </div>
  </div>

  <div class="form-row">
  <div class="form-group col-md-4">
    <label for="cuisine">Style de cuisine : </label>
      <select class="form-control" id="cuisine" name="cuisine">
         <option value="Italienne">Italienne</option>
         <option value="Française">Française</option>
         <option value="Japonaise">Japonaise</option>
         <option value="Indienne">Indienne</option>
         <option value="Africaine">Africaine</option>
      </select>
  </div>

    <div class="form-group col-md-4">
      <label for="prix">Prix moyen : </label>
      <input type="number" class="form-control" id="prix" name="prix">
   </div>
  <div class="form-group col-md-4">
      <label for="telephone">Téléphone : </label>
      <input type="text" class="form-control" id="telephone" name="telephone">
   </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-4">
    <label for="numero">Numéro de la rue : </label>
    <input type="text" class="form-control" id="numero" name="numero">
  </div>
  <div class="form-group col-md-8">
    <label for="rue">Nom de la rue : </label>
    <input type="text" class="form-control" id="rue" name="rue">
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-8">
      <label for="ville">Ville : </label>
      <input type="text" class="form-control" id="ville" name="ville">
    </div>
    <div class="form-group col-md-4">
      <label for="postale">Code postale : </label>
      <input type="text" class="form-control" id="postale" name="postale">
   </div>
  </div>

   <div class="form-row">
   <div class="form-group col-md-6">
      <label for="latitude">Latitude</label>
      <input type="text" class="form-control" id="latitude" name="latitude">
    </div>
    <div class="form-group col-md-6">
      <label for="longitude">Longitude</label>
      <input type="text" class="form-control" id="longitude" name="longitude">
    </div>
  </div>

  <button type="submit" class="btn btn-primary mt-4" name="creer">Créer un restaurant</button>
  <button type="submit" class="btn btn-primary mt-4" name="afficher">Afficher les restaurants</button>
</form>
</div>



<?php 
if (isset($_POST['creer'])) 
{
   $restaurant = [
      'name' => $_POST['nomRestaurant'],
      'cuisine' => $_POST['cuisine'],
      'site' => $_POST['siteWeb'],
      'numero' => $_POST['numero'],
      'rue' => $_POST['rue'],
      'ville' => $_POST['ville'],
      'postale' => $_POST['postale'],
      'prix' => $_POST['prix'],
      'telephone' => $_POST['telephone'],
      'latitude' => $_POST['latitude'],
      'longitude' => $_POST['longitude']
   ];
   $collection->insertOne($restaurant);
}

if(isset($_POST['delete']))
{
    $id = $_POST['delete'];
    $collection->deleteOne(
        ['_id' => new MongoDB\BSON\ObjectId($id)]
    );
}


if (isset($_POST['afficher'])) 
{
   $all = $collection->find();
?>

<div class="container w-50 my-5">
<table class="table table-dark">
  <thead>

<?php
   foreach ($all as $restaurant) 
   {
     echo " <tbody>
            <tr>
            <th scope=\"col\">Fiche du Restaurant</th>
            <th scope=\"col\">Nom du Restaurant</th>

            </tr>
         </thead>
            <tr>
            <td>
            <form action=\"fiche.php\" method=\"POST\">
            <input type=\"hidden\" name=\"fiche\"value=". $restaurant->_id . ">
            <input type=\"submit\" class=\"btn btn-primary\" value=\"Voir la fiche\">
            </form></td>
            <td>" . $restaurant['name'] . "</td>";
   }
?>
   </tbody>
</table>
</div>
<?php
}
?>
     <!-- Optional JavaScript -->
     <!-- jQuery first, then Popper.js, then Bootstrap JS -->
     <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>